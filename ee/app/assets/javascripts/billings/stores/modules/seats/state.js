export default () => ({
  isLoading: false,
  hasError: false,
  namespaceId: null,
  members: [],
  total: null,
  page: null,
  perPage: null,
});
